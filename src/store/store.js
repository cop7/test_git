import Vue from 'vue'
import Vuex from 'vuex'
import {utils} from "../constant/utils"
import {keys} from "../constant/keys"

Vue.use(Vuex)

const store = () => new Vuex.Store({

  state: {
    currentLocation: [],//面包屑的当前位置
    agentId: "",
    userId: "",
    userKey: ""
  },
  mutations: {
    //更新面包屑的位置
    changeCurrentLocation(state, pathArr) {
      state.currentLocation = pathArr;//更新当前位置
    },

    //更新登录的状态
    chageLoginState(state) {
      state.userKey = utils.getCookie(keys.USER_KEY);
      state.userId = utils.getCookie(keys.USER_ID);
      state.agentId = utils.getCookie(keys.AGENT_ID);
    }
  }
})

export default store
