import Vue from 'vue'
import VueRouter from 'vue-router'
import Container from "../components/pages/Container.vue"
import Index from "../components/pages/Index.vue"

import Seller_Partition from "../components/pages/Seller_Partition"
import Seller_List from "../components/pages/Seller_List"
import Seller_Add from "../components/pages/Seller_Add"
import Seller_Red_Bag from "../components/pages/Seller_Red_Bag"

import Rider_List from "../components/pages/Rider_List"
import Rider_Add from "../components/pages/Rider_Add"
import Rider_Order_Wait from "../components/pages/Rider_Order_Wait"
import Rider_Distri_State from "../components/pages/Rider_Distri_State"

import Order_List from "../components/pages/Order_List"
import Order_List_Wait from "../components/pages/Order_List_Wait "
import Order_Detail from "../components/pages/Order_Detail"

import Sys_Base_Info from "../components/pages/Sys_Base_Info"
import Sys_Income_Detail from "../components/pages/Sys_Income_Detail"
import Sys_Operator_List from "../components/pages/Sys_Operator_List"
import Sys_Operator_Auth from "../components/pages/Sys_Operator_Auth"
import Sys_Account_Info from "../components/pages/Sys_Account_Info.vue"

import Login from "../components/pages/Login"

Vue.use(VueRouter)

import {utils} from "../constant/utils"
import {keys} from "../constant/keys"
//路由映射配置
const routes = [
  {
    path: '/',
    redirect: '/container/index'
  },
  {
    path: '/container',
    component: Container,
    children: [
      {
        path: "index",
        component: Index
      },
      {
        path: "seller_list",
        component: Seller_List
      },
      {
        path: "seller_partition",
        component: Seller_Partition
      },
      {
        path: "seller_add",
        component: Seller_Add
      },
      {
        path: "rider_list",
        component: Rider_List
      },
      {
        path: "rider_order_wait",
        component: Rider_Order_Wait
      },
      {
        path: "seller_red_bag",
        component: Seller_Red_Bag
      },
      {
        path: "rider_add",
        component: Rider_Add
      },
      {
        path: "rider_distri_state",
        component: Rider_Distri_State
      },
      {
        path: "order_list",
        component: Order_List
      },
      {
        path: "order_list_wait",
        component: Order_List_Wait
      },
      {
        path: "order_detail",
        component: Order_Detail
      },
      {
        path: "sys_base_info",
        component: Sys_Base_Info
      },
      {
        path: "sys_income_detail",
        component: Sys_Income_Detail
      },
      {
        path: "sys_operator_list",
        component: Sys_Operator_List
      },
      {
        path: "sys_operator_auth",
        component: Sys_Operator_Auth
      },
      {
        path: "sys_account_info",
        component: Sys_Account_Info
      }
    ]
  },
  {
    path: "/login",
    component: Login
  }
]

//创建实例
const router = new VueRouter({
  routes
})

//全局导航钩子,路由进入之前
router.beforeEach((to, from, next) => {
  if (to.path === "/login") {
    next();
    return;
  }
  let userid = utils.getCookie(keys.USER_ID);
  let userKey = utils.getCookie(keys.USER_KEY);
  let agentId = utils.getCookie(keys.AGENT_ID);
  if (utils.isEmpty(userid) || utils.isEmpty(userKey) || utils.isEmpty(agentId)) {
    router.replace("/login");
    return;
  } else {
    next();
  }
})

router.afterEach(route => {
})


export default router
