import axios from "axios"

axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.withCredentials = true
import {utils} from "./utils";
import {keys} from "./keys";

let http = {

  //用于未登陆时接口返回参数
  requestData: function (that, parameter, sessiontype, success) {
    parameter.append("version", 1);
    parameter.append("sessiontype", sessiontype);//未登陆
    parameter.append("magic", "agent_h5_v100");
    axios.post(that.$api.host, parameter).then((res) => {
      that.loading = false;
      if (res.data.result === "ok") {
        success(res.data);
      } else if (!utils.isEmpty(res.data.tips)) {
        that.$message.error(res.data.tips);
      } else {
        that.$message.error(res.data.result);
      }
    }).catch((err) => {
      that.loading = false;
      that.$message.error(err);
    })
  },

  //用户已登录时获取接口参数
  requestAuthData: function (that, parameter, success) {
    let userid = utils.getCookie(keys.USER_ID);
    let userKey = utils.getCookie(keys.USER_KEY);
    let agentid = utils.getCookie(keys.AGENT_ID);
    let random = utils.getRandom();
    let randomKey = utils.getRandomkey(userid, userKey, random);
    if(that.$utils.isEmpty(userid)||that.$utils.isEmpty(userKey)){
      that.$router.replace('/');
      return;
    }
    parameter.append("version", 1);
    parameter.append("sessiontype", 3);//3代理商
    parameter.append("magic", "agent_h5_v100");
    parameter.append("agentid", agentid);
    parameter.append("userid", userid);
    parameter.append("random", random);
    parameter.append("randomkey", randomKey);
    axios.post(that.$api.host, parameter).then((res) => {
      that.loading = false;
      if (res.data.result === "ok") {
        success(res.data);
      } else if (!utils.isEmpty(res.data.tips)) {
        that.$message.error(res.data.tips);
      } else {
        that.$message.error(res.data.result);
      }
    }).catch((err) => {
      that.loading = false;
      that.$message.error(err);
    })
  },

  //初始化图片上传URL
  getDownLoadUrl: function (that,action) {
    let userid = utils.getCookie(keys.USER_ID);
    let userKey = utils.getCookie(keys.USER_KEY);
    let agentid = utils.getCookie(keys.AGENT_ID);
    let random = utils.getRandom();
    let randomKey = utils.getRandomkey(userid, userKey, random);
    return that.$api.host + "?action="+action+"&download=1" +
      "&version=1" + "&sessiontype=" + 3 + "&magic=agent_h5_v100" + "&userid="
      + userid + "&agentid=" + agentid + "&random=" + random + "&randomkey=" + randomKey;
  },


  //初始化图片上传URL
  initUploadImgURL: function (that) {
    let userid = utils.getCookie(keys.USER_ID);
    let userKey = utils.getCookie(keys.USER_KEY);
    let agentid = utils.getCookie(keys.AGENT_ID);
    let random = utils.getRandom();
    let randomKey = utils.getRandomkey(userid, userKey, random);
    return that.$api.host + "?action=base_photo_upload" +
      "&version=1" + "&sessiontype=" + 3 + "&magic=agent_h5_v100" + "&userid="
      + userid + "&agentid=" + agentid + "&random=" + random + "&randomkey=" + randomKey + "&size=2";
  },


  //上传文件
  uploadFileData: function (that, url, parameter, success) {
    let user_key = utils.getCookie(keys.USER_ID);
    let user_token = utils.getCookie(keys.USER_TOKEN);
    parameter.append("user_key", user_key);
    parameter.append("user_token", user_token);
    let config = {
      headers: {'Content-Type': 'multipart/form-data'}
    };
    axios.post(that.$api.host + url, parameter, config).then((res) => {
      that.loading = false;
      if (res.data.code === 200) {
        success(res.data);
      } else {
        that.$message.error(res.data.tips);
      }
    }).catch((err) => {
      that.loading = false;
      that.$message.error(err);
    })
  }
}

export {http}
