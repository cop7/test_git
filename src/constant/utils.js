import md5 from "js-md5"

let utils = {
  // 设置页面标题
  setDocTitle: function (title) {
    setTimeout(function () {
      document.title = title
      var iframe = document.createElement('iframe')
      iframe.src = '/manage/static/logo.png'
      iframe.style.visibility = 'hidden'
      iframe.style.width = '1px'
      iframe.style.height = '1px'
      iframe.onload = function () {
        setTimeout(function () {
          document.body.removeChild(iframe)
        }, 0)
      }
      document.body.appendChild(iframe)
    }, 0)
  },

  ///判断对象为undefined或null或""
  isEmpty: function (text) {
    if (typeof (text) === 'undefined' || text === null || text === '') {
      return true
    }
    return false
  },

  //验证手机号的正确性
  isPhoneNum: function (phone) {
    let reg = /(^0{0,1}1[3|4|5|6|7|8|9][0-9]{9}$)/
    if (reg.test(phone)) {
      return true
    } else {
      return false
    }
  },

  //检测是都为电话号码
  isTelephone: function (tele) {
    let re = /^0\d{2,3}-?\d{7,8}$/;
    if(re.test(tele)){
      return true
    }else{
      return false
    }
  },

  //验证邮箱的格式
  isEmail: function (email) {
    let reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    if (reg.test(email)) {
      return true
    } else {
      return false
    }
  },

  //会话级别存对象
  saveSessionObj: function (key, obj) {
    let str = JSON.stringify(obj)
    sessionStorage.setItem(key, str)
  },

  //会话级别取对象
  getSessionObj: function (key) {
    let str = sessionStorage.getItem(key)
    return JSON.parse(str)
  },


  //获取字符格式的日期，参数date类型
  getTimeDt: function (dt) {
    if (typeof (dt) === 'undefined' || dt === null || dt === '') {
      return '';
    }
    let nowDate = dt;
    let year1 = nowDate.getFullYear();
    let month1 = nowDate.getMonth() + 1;
    let date1 = nowDate.getDate();
    let hours1 = nowDate.getHours();
    let minutes1 = nowDate.getMinutes();
    let seconds = nowDate.getSeconds();
    if (month1 < 10) {
      month1 = "0" + month1;
    }
    if (date1 < 10) {
      date1 = "0" + date1;
    }
    if (hours1 < 10) {
      hours1 = "0" + hours1;
    }
    if (minutes1 < 10) {
      minutes1 = "0" + minutes1;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return year1 + "-" + month1 + "-" + date1 + " " + hours1 + ":" + minutes1 + ":" + seconds;
  },


  //设置Cookie,至少传name和value
  setCookie: function (name, value, lostTime, path) {
    let Cookietime = new Date();
    Cookietime.setDate(Cookietime.getDate() + lostTime);
    document.cookie = name + '=' + value + ';expires=' + Cookietime + ";path=" + path;
  },

  //获取浏览器端Cookie
  getCookie: function (name) {
    let arr = window.document.cookie.split('; ');
    for (let i = 0; i < arr.length; i++) {
      let arr2 = arr[i].split('=');
      if (arr2[0] == name) {
        return arr2[1];
      }
    }
    return "";
  },

  //删除Cookie
  removeCookie: function (name, path) {
    let value;
    let arr = window.document.cookie.split('; ');
    for (let i = 0; i < arr.length; i++) {
      let arr2 = arr[i].split('=');
      if (arr2[0] == name) {
        value = arr2[1];
      }
    }
    let exp = new Date();
    exp.setTime(exp.getTime() - 1);
    document.cookie = name + "=" + value + ";expires=" + exp.toGMTString()
  },

  //将毫秒值转为时间格式
  millisToTime(millis) {
    let date = new Date(millis);
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  },

  /**
   * 生成一个 0 - 1000 之间的随机整数
   * @returns {Number}
   */
  getRandom: function () {
    let s = []
    let hexDigits = '0123456789abcdef'
    for (let i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)
    }
    s[14] = '4'  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1)  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = '-'
    let uuid = s.join('')
    let tm = parseInt(new Date().getTime() / 1000)
    return uuid + '-' + tm
  },

  /**
   * 获取随机键
   * @param random
   * @returns {*}
   */
  getRandomkey: function (userid, userkey, random) {
    return md5('' + userid + random + userkey + userid + random + userkey)
  },

  //拼接图片地址
  genImgURL: function (url) {
    let item = localStorage.getItem("systerm_info");
    let info = JSON.parse(item);
    return info.base.imagebase + "/" + url;
  }
}


export {utils}
