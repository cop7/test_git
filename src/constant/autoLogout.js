let autoLogout={
  currentMousePosition1:'',//记录鼠标坐标
  currentMousePosition2:'',
  eventID:'',

  timeout: 30*60*1000,  //设定超时时间
  currentSecond :0,

  GetXYPosition:function(e){
    let target = e || window.event;
    let point={};
    point.x=target.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    point.y=target.clientY + document.body.scrollTop + document.documentElement.scrollTop
    this.currentMousePosition2 = point;
  },

  CheckTime :function () {
    console.log(window.autoLogout.currentMousePosition1,'===',window.autoLogout.currentMousePosition2)
    if(window.autoLogout.currentMousePosition1 == window.autoLogout.currentMousePosition2) {
      window.autoLogout.currentSecond = window.autoLogout.currentSecond+1000;
      if(window.autoLogout.currentSecond > window.autoLogout.timeout) { //已超时
        clearInterval(window.autoLogout.eventID);
        let exp = new Date();
        exp.setTime(exp.getTime() - 1);
        document.cookie = 'user_id=;expires=' + exp.toGMTString();
        document.cookie = 'user_key=;expires=' + exp.toGMTString();
        document.cookie = 'phoneNum=;expires=' + exp.toGMTString();
        document.cookie = 'agent_id=";expires=' + exp.toGMTString();
      }
    } else {
      window.autoLogout.currentSecond = 0;
      window.autoLogout.currentMousePosition1 = window.autoLogout.currentMousePosition2;
    }
  },

  StartPoint:function(){
    window.autoLogout=this;
    this.self=this;
    this.currentMousePosition1 = "";
    this.currentMousePosition2 = "";
    this.eventID = setInterval(this.CheckTime,1000);
    document.getElementsByTagName('body')[0].addEventListener('mousemove',function (ev) {
      window.autoLogout.GetXYPosition(ev);
    })
  }
}
export {autoLogout}
