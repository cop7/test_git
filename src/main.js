// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import  'babel-polyfill'
import 'url-search-params-polyfill';
import Vue from 'vue'
import App from './App'
import router from './router/index'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import {api} from "./constant/api"
import {keys} from "./constant/keys"
import {utils} from "./constant/utils"
import {pathno} from "./constant/pathno"
import {http} from "./constant/http"
import store from "./store/store"

Vue.config.productionTip = false

Vue.use(ElementUI)


Vue.prototype.$api = api;
Vue.prototype.$utils = utils;
Vue.prototype.$pathno = pathno;
Vue.prototype.$http = http;
Vue.prototype.$keys = keys;


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
})
